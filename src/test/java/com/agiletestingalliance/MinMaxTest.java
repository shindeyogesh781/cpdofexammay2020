package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

   public class MinMaxTest {

     	@Test
      	public void testBar() throws Exception {
         	String k =new MinMax().bar("Yogesh");
	         assertEquals("BarMax","Yogesh",k);
	}

	@Test
	public void testBarForNull() throws Exception {
         	String k =new MinMax().bar("");
	         assertEquals("BarMin","",k);
        }
	
/*	@Test
      	public void testBarForEmpty() throws Exception {
		String k = new MinMax().bar(null);
		assertEquals("BarNull",null,k);
	}*/
	
	@Test
	public void testfForMax() throws Exception {
		int k = new MinMax().f(3,5);
		assertEquals("fa",5,k);
	}

	@Test
		public void testfForMin() throws Exception {
		int k = new MinMax().f(5,3);
		assertEquals("fb",5,k);
	}
}

